package moneytalks.trades

import static org.junit.Assert.*

import javax.swing.JOptionPane;


import moneytalks.money.Money
import moneytalks.user.Profile
import moneytalks.user.User

import org.junit.*
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder

class TradesServiceIntegrationTests {

	def authenticationManager

	def tradesService

	def user

	@Before
	void setUp() {
		def totalSpends =  createSpends()
		def totalIncomes = createIncomes()
		
		def profile = new Profile(spends : totalSpends, incomes : totalIncomes , 
								  name : "teste")
		
		user = new User(username : "teste",
						password : "teste",
						profile : profile,
						enabled : true)	
		user.save()

		if(user.hasErrors()) {
			user.errors.allErrors.each {println it}
			fail(user.errors.allErrors)
		}

		
		authenticate()
	}

	private authenticate() {
		def token = new UsernamePasswordAuthenticationToken("teste" , "teste")

		def auth  = authenticationManager.authenticate(token)

		SecurityContextHolder.getContext().setAuthentication(auth)
	}

	@After
	void tearDown() {}

	@Test
	void test_lista_todos_os_valores_de_spend_de_usuario() {
		def quantity = tradesService.sumSpends()
		assert new Money(60.00).value  == quantity.value
	}
	
	
	@Test
	void test_lista_todos_os_valores_de_income_de_usuario() {
		def quantity = tradesService.sumIncomes()
		assert new Money(80).value  == quantity.value
	}

	private def createSpends(){
		def firstSpend  = new Spends(value : new Money(20),
									 title : "Teste")
		def secondSpend  = new Spends(value : new Money(40),
									  title : "Teste")
		[firstSpend , secondSpend]
	}
	
	private def createIncomes(){
		def firstIncome = new Incomes(title : "Teste",
									  value : new Money(40))
		def secondIncome = new Incomes(title : "Teste",
									  value : new Money(40))
		[firstIncome , secondIncome]
	}
}