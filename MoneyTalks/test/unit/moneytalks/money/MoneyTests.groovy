package moneytalks.money


import junit.framework.TestCase;


import moneytalks.money.Money;


import org.junit.*

class MoneyTests extends TestCase{

	def money
	
	@Before
	void setUp(){
		money = new Money(1000.0)
	}
	
    void test_formatacao_real() {
		assert 'R$ 1.000,00' == money.format()
	}
	
	void test_operacao_soma_sem_decimal(){
		assert 50 == (new Money(25) + new Money(25)).value
	}
	
	void test_operacao_soma_com_decimal(){
		assert 50.99 == (new Money(25.50) + new Money(25.49)).value
	}
	
	void test_operacao_soma_com_locale(){
		assert 50.99 == (new Money("25,50") + new Money("25,49")).value
	}
	
	void test_operacao_subtracao(){
		assert 0.50 == (new Money("25,50") - new Money("25")).value
	}
	
	void test_operacao_divisao(){
		assert 5 == (new Money("15") / new Money("3")).value
	}
	
	void test_operacao_multiplicacao(){
		assert 45 == (new Money("15") * new Money("3")).value
	}
		
}