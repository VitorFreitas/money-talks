package moneytalks.trades

import grails.test.mixin.*
import grails.test.mixin.support.*
import moneytalks.mocks.UserMock
import moneytalks.money.Money

import org.junit.*


@TestMixin(GrailsUnitTestMixin)
@TestFor(Incomes)
class IncomesTests {

	@Delegate UserMock user = new UserMock()
	
    void testSaveSimpleIncome() {
        def income = createIncomes()[0]
		
		assertTrue income.validate()
		assert income.save() != null
		assertNull Incomes.get(1).source
		
		income.source = IncomesType.INVESTIMENT
		assert income.save() != null
		assertNotNull Incomes.get(1).source
    }
	
	void testValidateIncome(){
		def income = createIncomes()[0]
		
		income.value = new Money(-20)
		assertFalse income.validate()
		income.value = new Money(20)
		
		income.title = ""
		assertFalse income.validate()
	}
}
