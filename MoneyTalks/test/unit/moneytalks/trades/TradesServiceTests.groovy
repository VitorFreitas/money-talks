
package moneytalks.trades




import grails.test.mixin.*
import moneytalks.mocks.UserMock
import moneytalks.money.Money
import moneytalks.user.Profile
import moneytalks.user.User

import org.junit.*



@TestFor(TradesService)
@Mock([User , Profile , Trade , Spends , Incomes])
class TradesServiceTests {

	@Delegate UserMock user = new UserMock()

	User currentUser
	
	void setUp(){
		currentUser = user.saveUser(1 , true)
		service.user = {currentUser}
	}
		
	void test_lista_os_spends_e_incomes_de_cada_usuario(){				
		assert service.allSpends().size() == 2
		assert service.allIncomes().size() == 2
	}

	void test_save_trades(){
		service.saveIncome(createIncomes(1)[0])
		service.saveSpend(createSpends(1)[0])
		assert service.allSpends().size() == 3
		assert service.allIncomes().size() == 3
	}
	
	void test_soma_todos_trades(){
		assert service.sumSpends() == new Money(60)
		assert service.sumIncomes() == new Money(80)
	}	
	
}
