package moneytalks.trades

import static org.junit.Assert.*
import grails.test.mixin.*
import grails.test.mixin.support.*
import groovy.lang.Delegate;
import moneytalks.mocks.UserMock;

import org.junit.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@TestFor(Spends)
class SpendsTests {

	@Delegate UserMock user = new UserMock()
	
	void testSaveSimpleSpends() {
		Spends spends = createSpends()[0]
		
		assertTrue spends.validate()		
		assertNotNull spends.save()
		assertFalse Spends.get(1).unexpected
		
		spends.source = "Teste"
		spends.unexpected = true
		
		assertNotNull spends.save() != null
		
		assert Spends.get(1).source == "Teste"
		assertTrue Spends.get(1).unexpected
    }
}