package com.moneytalks

import com.grailsrocks.functionaltest.*

class HomeTestFunctionalTests extends BrowserTestCase {

	void testAjaxCalltoIncome() {
		login()
		
		assertElementTextContains "incomeTotal" , "80"

		form("formIncome") {
			titleIncome = "Test"
			incomeValue = "123"
			submitIncome.click()
		}

		//ajax
		Thread.sleep(500)

		assertElementTextContains "incomeTotal" , "203"
	}
	
	void testAjaxCalltoSpend() {
		login()
		
		assertElementTextContains "spendTotal" , "60"

		form("formSpend") {
			titleSpend = "Test"
			spendValue = "20"
			saveSpend.click()
		}

		//ajax
		Thread.sleep(500)

		assertElementTextContains "spendTotal" , "80"
	}
	
	
	def login(){
		get('/login/auth')
		
		form("loginForm"){
			j_username "user1"
			j_password "user1"
			click "submit"
		}
		
		get('/home/listAll')

	}
}
