package moneytalks.mocks

import moneytalks.money.Money
import moneytalks.trades.Incomes
import moneytalks.trades.Spends
import moneytalks.user.Profile
import moneytalks.user.User

class UserMock {

	def saveUser = { id , shouldMockSecurity = false ->
		def user = createUser(id)

		if(user.hasErrors()) {
			user.errors.allErrors.each { log.error(it) }
			throw new RuntimeException("Usuario invalido : $it")
		}
	
		if ( shouldMockSecurity) user.springSecurityService = new SecurityMock()
		
		user.save(flush:true)
		user
	}
	
	def createUser = {
		def profile = createProfile(it)
		def user = new User(username : profile.name , 	password : profile.name , profile : profile, enabled : true)
		user
	}

	def createProfile =  { 
		def totalSpends =  createSpends()
		def totalIncomes = createIncomes()
		def profile = new Profile(spends : totalSpends, incomes : totalIncomes ,  name : "user$it")

		return profile
	}


	private def timeLabel = {
		"${it}${System.currentTimeMillis()}"
	}
	
	def createSpends = {
		def firstSpend  = new Spends(value : new Money(20),	 title : timeLabel("spend"))
		def secondSpend  = new Spends(value : new Money(40),  title : timeLabel("spend"))
		[firstSpend , secondSpend]
	}
	
	def createIncomes = {
		def firstIncome = new Incomes(title : timeLabel("income"), value : new Money(40))
		def secondIncome = new Incomes(title : timeLabel("income"), value : new Money(40))
		[firstIncome , secondIncome]
	}

}
