package moneytalks.mocks

import grails.plugins.springsecurity.SpringSecurityService;

class SecurityMock extends SpringSecurityService {

	@Override
	public String encodePassword(String password) {
		"encpass:$password"
	}
}
