package moneytalks.money

import java.text.NumberFormat

class Money{

	BigDecimal value
	
    public Money(BigDecimal value) {
		this.value = value;
	}
	
	public Money(){}
	
	public Money(String value){
		this.value = parseValue(value)
	} 
	
	private parseValue(value){
		new BigDecimal(formatDecimal().parse(value).doubleValue()).setScale(2 , BigDecimal.ROUND_HALF_UP);
	}
		
	def format(){
		formatCurrency(new Locale("pt" , "BR")).format(value)		
	}
		
	private formatDecimal(){
		def decimal = NumberFormat.getInstance(new Locale("pt" , "BR"))
		decimal.maximumFractionDigits = 2
		decimal.minimumFractionDigits = 2
		decimal
	}
	
	private formatCurrency(locale){
		NumberFormat.getCurrencyInstance(locale)
	}
	
	static constraints = {
		value(min : 0.0)
    }

	def plus(money){
		new Money(this.value + money.value); 
	}
	
	def minus(money){
		new Money(this.value - money.value);
	}
	
	def div(money){
		new Money(this.value / money.value);
	}
	
	def multiply(money){
		new Money(this.value * money.value);
	}

	int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object money) {
		value.equals(money.value)
	}	
	
	String toString(){
		format()
	}
}
