package moneytalks.user

import moneytalks.trades.Spends;

class User {

	Profile profile
		
	String username
	
	String password
	
	boolean enabled
	
	boolean accountExpired
	
	boolean accountLocked
	
	boolean passwordExpired

	transient springSecurityService

	static constraints = {}

	static mapping = {
		password column: '`password`'
		profile cascade: "all" 
		profile(lazy:false)
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
