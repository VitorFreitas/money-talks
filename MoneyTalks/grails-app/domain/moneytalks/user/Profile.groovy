package moneytalks.user

import moneytalks.trades.Spends
import moneytalks.trades.Incomes



class Profile {

	String name

	List spends
	
	List incomes
	
	static belongsTo = [user : User]
	
	static hasMany = [spends : Spends , incomes : Incomes]
	
    static constraints = {}
	
	static mapping = {
		user 	cascade: "all"
		spends  cascade: "all"
		incomes cascade: "all"
	}
	
}
