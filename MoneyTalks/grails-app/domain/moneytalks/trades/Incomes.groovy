package moneytalks.trades


import moneytalks.money.Money

class Incomes extends Trade {

	IncomesType source
	
	Boolean liquid = false
	
	static constraints = {
		source(nullable : true)
	}
	
	
}
