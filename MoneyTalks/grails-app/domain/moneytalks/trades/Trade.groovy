package moneytalks.trades


import moneytalks.money.Money;
import moneytalks.user.Profile

abstract class Trade {
	
	String title
	
	Money value
	
	Date dateCreated
	
	static embedded = ['value']

	static constraints = {
		value(nullable : false , validator : {it.value > 0})
		title(nullable : false , blank : false)
	}
	
	static mapping = {
		 tablePerSubclass true		 	
	}
	
	static Money sumAll(){
		
	}
	
}
