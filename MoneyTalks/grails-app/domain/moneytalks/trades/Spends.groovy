package moneytalks.trades

class Spends extends Trade {

	String source
	
	Boolean unexpected = false
	
	static constraints = {
		source(nullable: true , blank : true)
	}
}
