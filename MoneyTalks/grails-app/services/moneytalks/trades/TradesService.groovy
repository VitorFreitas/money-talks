package moneytalks.trades

import moneytalks.money.*
import moneytalks.user.Profile

class TradesService {

	def springSecurityService
	
	def user = {springSecurityService.currentUser}
	
	def sumAll = { tradeName ->		
		def trades = user().profile."$tradeName"
		def values = trades.collect({it.value})		
		return values?.inject { current , total -> current + total }
	}	
	
	def saveIncome(income){
		Profile profile = user().profile
		profile.incomes << income
		profile.save()		
	}
	
	def sumSpends(){ 
		return sumAll("spends")
	}
	
	def sumIncomes() { 
		sumAll("incomes")
	}
	
	def saveSpend(spend){
		Profile profile = user().profile
		profile.spends << spend
		profile.save()
	}
	
	def getMinimunDate(){			
		def calendarMinimun = Calendar.getInstance()		
		calendarMinimun.set(Calendar.DAY_OF_MONTH, calendarMinimun.getActualMinimum(Calendar.DAY_OF_MONTH))
		calendarMinimun.getTime()
	}
	
	def getMaximunDate(){
		def calendarMax = Calendar.getInstance()
		calendarMax.set(Calendar.DAY_OF_MONTH, calendarMax.getActualMaximum(Calendar.DAY_OF_MONTH))
		calendarMax.getTime()
	}
	
	def now(){
		def calendar = Calendar.getInstance()
		calendar.setTime(new Date())
	}
	
	def allSpends(){
		user().profile.spends
	}
	
	def allIncomes(){
		user().profile.incomes
	}
}
