package moneytalks.home

import grails.plugins.springsecurity.Secured
import moneytalks.money.Money
import moneytalks.trades.Spends
import moneytalks.trades.Incomes


@Secured(['IS_AUTHENTICATED_FULLY'])
class HomeController {

	def tradesService
	
	def listAll = {
		render(view : 'listAll' , model:[spends : tradesService.sumSpends() , 
					  			  		incomes : tradesService.sumIncomes() , 
										listIncomes : tradesService.allIncomes(),
										listSpends : tradesService.allSpends()])
	}
	
	def index = {
		redirect action : 'listAll'
	}

	def saveIncome = {
		tradesService.saveIncome(new Incomes(title : params.titleIncome , value : new Money(params.incomeValue)))
		render tradesService.sumIncomes().format()
	} 	
	
	def saveSpend = {
		tradesService.saveSpend(new Spends(title: params.titleSpend , value : new Money(params.spendValue)))
		render tradesService.sumSpends().format()
	}
	
	def listIncomes =  {
		tradesService.allIncomes()
	}
	
	def listSpends =  {
		tradesService.allSpends()
	}
	
}
