<head>
	<meta name="layout" content="default_layout"/>
	<r:require module="core"/>
	<title><g:message code="springSecurity.login.title"/></title>
</head>

<body>
	<section id="main">
			<article class="gbox boxtype1" draggable="true">
				<h1>Entrada</h1>
				<form class="fieldtype2" action="saveIncome" id="formIncome">
					<fieldset>
						<label> Titulo: </label>
						<input type="text" name="titleIncome" id="titleIncome"/>
						
						<label> Valor: </label>
						<input type="text" name="incomeValue" id="incomeValue"/>						
					</fieldset>
					
					<section>
						<p> Total : <span id="incomeTotal">${incomes.format()}</span> </p>
					</section>
						
						<g:submitToRemote id="submitIncome" name="submitIncome" action="saveIncome" update="incomeTotal" value="submitIncome" style="cursor: pointer;"></g:submitToRemote>  
					
					<fieldset>
					<g:each in="${listIncomes}" var="income">
						<label> ${income.title} - ${income.value.format()} , ${income.dateCreated} </label>
					</g:each>
					</fieldset>
				</form>
				</article>
				
				
				<article class="gbox boxtype1" draggable="true">
				<h1>Saida</h1>
				<form class="fieldtype2" action="saveSpend" name="formSpend">
					<fieldset>
						<label> Titulo: </label>
						<input type="text" id="titleSpend" name="titleSpend" />
						<label> Valor: </label>
						<input type="text" id="spendValue" name="spendValue" />						
					</fieldset>
					
					<section>
						<p> Total : <span id="spendTotal">${spends.format()}</span> </p>  
					</section>
					
					<g:submitToRemote action="saveSpend" update="spendTotal" value="saveSpend" name="saveSpend"></g:submitToRemote>
					
					<fieldset>
					<g:each in="${listSpends}" var="spend">
						<label> ${spend.title} - ${spend.value.format()} , ${spend.dateCreated} </label>
					</g:each>
					</fieldset>
				</form>
				</article>
				
				
				<article class="gbox boxtype1" draggable="true">
				<h1>Mes atual:</h1>
				<form class="fieldtype2" action="saveSpend">
						<fieldset>
							<g:each in="${trades}"></g:each>
						</fieldset>
					<section>
						<p> Total : <span id="spendTotal">${tradeTotal}</span> </p>  
					</section>
					
					<g:submitToRemote action="saveSpend" update="spendTotal" value="update" />
				</form>
				</article>
		</section>
</body>