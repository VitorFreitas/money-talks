<head>
	<meta name="layout" content="default_layout"/>
	<r:require module="core"/>
	<title><g:message code="springSecurity.login.title"/></title>
</head>

<body>

	<section id="main">
			<article class="gbox">
				<h1><g:message code="springSecurity.login.header"/></h1>
				
					<g:if test='${flash.message}'>
						<div class='login_message'>${flash.message}</div>
					</g:if>
					<form action='${postUrl}' method='POST' id='loginForm' class="fieldtype1">
						<fieldset>
							<label for="j_username"><g:message code="springSecurity.login.username.label"/>: </label>
							<input type="text" name="j_username" class="text" />	
						</fieldset>
						<fieldset>
							<label for="password"><g:message code="springSecurity.login.password.label"/>: </label>
							<input type="password" name='j_password' id='password' class="text" />	
						</fieldset>
						
							<label for="${rememberMeParameter}"><g:message code="springSecurity.login.remember.me.label"/></label>
							<input type='checkbox' name='${rememberMeParameter}' id='remember_me'<g:if test='${hasCookie}'>checked='checked'</g:if> />
						
							
					<input type="submit" id="submit" value="${message(code: "springSecurity.login.button")}" class="botao1" />
					</form>			
			</article>
	</section>
		
<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>