<!DOCTYPE HTML>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Money Talks"/></title>
		<g:layoutHead />
        <r:layoutResources />
	</head>
	<body>
	<g:include view="fragments/header/defaultHeader.gsp" />
	    <g:layoutBody />
        <r:layoutResources />
	</body>
</html>