import moneytalks.mocks.UserMock;

class BootStrap {

    def init = { servletContext ->
		new UserMock().saveUser(1)
		new UserMock().saveUser(2)
    }

    def destroy = {}
}
